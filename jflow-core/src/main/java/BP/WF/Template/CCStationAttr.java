package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import BP.WF.*;
import java.util.*;

//using BP.ZHZS.Base;


/** 
 抄送到岗位 属性	  
*/
public class CCStationAttr
{
	/** 
	 节点
	*/
	public static final String FK_Node = "FK_Node";
	/** 
	 工作岗位
	*/
	public static final String FK_Station = "FK_Station";
}